# COS

## Installation

Download or clone repo 

```bash
git clone https://gitlab.com/3flippa/cos.git
```
Install your configuration

On Ubuntu

```bash
sudo bash ./main.sh
```

On Windows

```ps1
./main.bat
```

## Structure

Stages:
1. Stage 1 - Dependencys and Commands
2. Stage 2 - Programs and GUIs 
3. Stage 3 - Set Custom Configs

## Dir
#### DevTools
the place for installation folders when the option for a custom installation path is given e.g. Android SDK

#### Develop
the place where code lives and thrives. here u clone ur git repos

## Technologies 
### Linux
#### Supported
- curl
- git
- neofetch
- vim
- zsh
- terminal config

#### Support added in the future
- docker
- git repos
- flutter
- vs code
- chrome
- neovim
- java
- python
- minecraft
- nvm, node, npm
- rpi-imager
- balena etcher
- cubic

### Windows
- start building osm for windows
