#!/bin/bash

RED='\033[0;31m'
GREEN='\033[0;32m'
YELLOW='\033[0;33m'
NC='\033[0m' # No color

source functions/get_name_and_path.sh
source functions/install_packages.sh

package_names=()
package_paths=()

get_name_and_path "./packages"
for package_name in "${package_names[@]}"
do
    echo "$package_name"
done
install_packages "$package_names" "$package_paths"