#!/bin/bash

# Installation
apt-get install -y zsh
sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)" -o /home/flippa/DevTools

# Modification
chsh -s $(which zsh) # Change default shell to zsh
