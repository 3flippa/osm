#!/bin/bash

# Installation
apt-get install -y neofetch

# Modification
neofetch --ascii_colors 6 7 --colors 2 2 2 2
