#!/bin/bash

get_name_and_path() {
    local dir="$1"

    echo -e "${YELLOW}Collecting Packages${NC}"

    # Loop through each stage folder
    for stage_folder in packages/stage-*; do
        # Get the stage number from the folder name
        stage="${stage_folder##*-}"

        # Loop through each bash script in the stage folder
        for script_file in "$stage_folder"/*.sh; do
        # Check if it's a file
        if [[ -f "$script_file" ]]; then
            # Get the script name without the .sh ending
            script_name="${script_file##*/}"
            script_name="${script_name%.*}"

            # Add the script name and path to the respective arrays
            package_names+=("$script_name")
            package_paths+=("$script_file")
        fi
        done
    done
}




