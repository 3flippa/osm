#!/bin/bash

install_packages() {
  echo -e "${YELLOW}Updateing and installing packages${NC}" 
  # Get the length of the arrays
  local num_packages=${#package_names[@]}

  # Check if the arrays have the same length
  if [ ${#package_paths[@]} -ne $num_packages ]; then
    echo -e "${RED}Error: The package_names and package_paths arrays must have the same length.${NC}"
    return 1
  fi

  # Iterate over the arrays using a for loop
  for ((i=0; i<$num_packages; i++)); do
    echo -e "${YELLOW}Update and Upgrade${NC}"
    apt-get update
    apt-get upgrade -y
    echo -e "${GREEN}Installing ${package_names[$i]}...${NC}"
    source "${package_paths[$i]}"
    verification=(which ${package_names[$i]})
    if which "${package_names[$i]}" >/dev/null 2>&1; then
        echo -e "${GREEN}${package_names[$i]} is installed${NC}"
    else
        echo -e "${RED}${package_names[$i]} is not installed${NC}"
    fi
  done
}

